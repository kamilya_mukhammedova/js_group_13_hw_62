import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GamesService } from './shared/games.service';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewGameComponent } from './new-game/new-game.component';
import { FormsModule } from '@angular/forms';
import { GamePlatformComponent } from './game-platform/game-platform.component';
import { GameListComponent } from './game-platform/game-list/game-list.component';
import { DetailsComponent } from './game-platform/game-list/details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewGameComponent,
    GamePlatformComponent,
    GameListComponent,
    DetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [GamesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
