import { Component, OnInit } from '@angular/core';
import { Game } from '../../../shared/game.model';
import { GamesService } from '../../../shared/games.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  games!: Game[];
  neededGame!: Game[];

  constructor(
    private gameService: GamesService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.gamesChanges.subscribe((game: Game[]) => {
      this.games = game;
    });

    this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      this.neededGame = this.games.filter(game => game.id === id);
    });
  }
}
