import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GamesService } from '../../shared/games.service';
import { Game } from '../../shared/game.model';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {
  platformGamesList: Game[] = [];

  constructor(
    private route: ActivatedRoute,
    private gameService: GamesService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
        const platformId = parseInt(params['id']);
        const platformName = this.gameService.platforms[platformId];
        this.platformGamesList = this.gameService.getGamesByPlatform(platformName);
    });
  }
}
