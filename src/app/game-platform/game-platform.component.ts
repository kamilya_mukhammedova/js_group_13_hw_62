import { Component, OnInit } from '@angular/core';
import { GamesService } from '../shared/games.service';
import { Game } from '../shared/game.model';

@Component({
  selector: 'app-game-platform',
  templateUrl: './game-platform.component.html',
  styleUrls: ['./game-platform.component.css']
})
export class GamePlatformComponent implements OnInit {
  platforms: string[] = [];

  constructor(private gameService: GamesService) { }

  ngOnInit(): void {
    this.platforms = this.gameService.platforms;
  }
}
