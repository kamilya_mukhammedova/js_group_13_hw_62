import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGameComponent } from './new-game/new-game.component';
import { GamePlatformComponent } from './game-platform/game-platform.component';
import { GameListComponent } from './game-platform/game-list/game-list.component';
import { DetailsComponent } from './game-platform/game-list/details/details.component';

const routes: Routes = [
  {path: 'new-game', component: NewGameComponent},
  {path: '', component: GamePlatformComponent, children: [
      {path: ':id', component: GameListComponent},
    ]},
  {path: 'details/:id', component: DetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
