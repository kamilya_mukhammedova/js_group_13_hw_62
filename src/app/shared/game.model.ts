export class Game {
  id = (this.name + this.platform).replace(/\s+/g, '');
  constructor(
    public name: string,
    public image: string,
    public platform: string,
    public description: string,
  ) {}
}
