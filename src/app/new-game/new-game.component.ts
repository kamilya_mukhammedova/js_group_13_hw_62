import { Component, ElementRef, ViewChild } from '@angular/core';
import { GamesService } from '../shared/games.service';
import { Game } from '../shared/game.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('platformSelect') platformSelect!: ElementRef;
  @ViewChild('descriptionArea') descriptionArea!: ElementRef;

  constructor(private gameService: GamesService,
              private router: Router
  ) { }

  createNewGame() {
    const name = this.nameInput.nativeElement.value;
    const image = this.imageInput.nativeElement.value;
    const platform = this.platformSelect.nativeElement.value;
    const description = this.descriptionArea.nativeElement.value;
    if(name !== '' && image !== '' && description !== '') {
      const newGame = new Game(name, image, platform, description);
      this.gameService.addGame(newGame);
      this.nameInput.nativeElement.value = '';
      this.imageInput.nativeElement.value = '';
      this.platformSelect.nativeElement.value = 'NES';
      this.descriptionArea.nativeElement.value = '';
      void this.router.navigate(['']);
    } else {
      alert('You need to fill necessarily all the fields!');
    }
  }
}
